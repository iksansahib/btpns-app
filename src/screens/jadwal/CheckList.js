import React, { Component } from 'react';
import { 
    Header, 
    Button, 
    Body, 
    Title, 
    Right, 
    Icon, 
    Left,
    Text,
    View,
    Container,
    ListItem,
    CheckBox,
    List,
    Content,
    Spinner,
    Card,
    CardItem,
    Textarea
} from 'native-base';
import Axios from 'axios';
import { Image, Alert, TouchableOpacity } from 'react-native';
import { url_api } from '../../config';
import Modal from 'react-native-modal';

const smileImage = require('../../../assets/smile.png');
const sadImage = require('../../../assets/sad.png');

const imageStyle = {
    overflow:'hidden', 
    height:50, 
    width:50, 
};

const containerStyle = {
    borderRadius:10, 
    overflow:'hidden', 
    height:60, 
    width:60, 
    alignItems:'center',
    alignSelf: 'stretch', 
    justifyContent:'center',
    marginRight:20
};

const modalContentStyle = {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    height: 300,
    borderColor: "rgba(0, 0, 0, 0.1)"
}

export default class CheckList extends Component{
    constructor(){
        super();
        this.state = {
            saving: false,
            m_jadwal_id: 0,
            checklist: [],
            modalRating: false,
            kategori_id: null,
            komentar: null,
            preventive: null,
            corrective: null
        }
    }
    componentWillMount(){
        const { navigation } = this.props;
        const m_jadwal_id = navigation.getParam('m_jadwal_id');
        const kategori_id = navigation.getParam('kategori_id');

        this.setState({
            m_jadwal_id: m_jadwal_id,
            checked: [],
            kategori_id: kategori_id
        });
    }

    componentDidMount(){
        if(this.state.kategori_id==67){
            return false;
        }
        Axios.get(`${url_api}/api/checklist`)
            .then((res)=>{
                this.setState({ checklist: res.data });
                newChecked = [];
                res.data.map((item)=>{
                    newChecked[item.m_checklist_id] = false;
                });
                this.setState({checked : newChecked});
            })
            .catch((err)=>alert(err));
    }
    save(){
        this.setState({saving:true});
        if(this.state.kategori_id==66){
            check = this.state.checked.some((item)=>{
                if(!item){
                    alert('Checklist Belum Lengkap');
                    return true;
                }
                return false;
            });
            if(check){
                this.setState({
                    saving:false,
                    modalRating: false
                });
                return false;
            }
        }
        Axios.post(`${url_api}/api/checklist`, {
            m_jadwal_id: this.state.m_jadwal_id,
            puas: this.state.rating,
            komentar: this.state.komentar,
            preventive: this.state.preventive,
            corrective: this.state.corrective
        })
        .then((res) => {
            console.log(res);
            if(res.data.success){
                Alert.alert('Sukses', 
                    res.data.msg,
                    [{text: 'OK', onPress: (() => this.props.navigation.goBack())}]
                );
            }
            this.setState({
                saving:false,
                modalRating: false
            });
        })
        .catch((err) => {
            this.setState({
                saving:false,
                modalRating: false
            });
            alert(err);
        });

    }

    getChild(item){
        return (
            item.child.map(child=>{
                return (
                    <Text key={child.m_checklist_id} note>{child.name}</Text>
                )
            })
        )
    }
    handleChange(id){
        newChecked = this.state.checked;
        newChecked[id] = !this.state.checked[id];
        this.setState({checked: newChecked});
    }
    getCheckList(){
        if(this.state.kategori_id==67){
            return <Text>Tidak ada Checklist untuk Motor</Text>
        } else {
            return (
                this.state.checklist.map(item=>{
                    return (
                        <ListItem key={item.m_checklist_id}>
                            <Body>
                                <Text>{item.name}</Text>
                                <View>{this.getChild(item)}</View>
                            </Body>
                            <Right>
                                <CheckBox 
                                    checked={this.state.checked[item.m_checklist_id]} 
                                    onPress={()=>this.handleChange(item.m_checklist_id)} />
    
                            </Right>
                        </ListItem>
                    )
                })
            )     
        }
    }

    showSpinner(){
        if(this.state.checklist.length==0 && this.state.kategori_id==66){
            return <Spinner />
        }
    }

    showRating(){
        if(this.state.kategori_id==66){
            check = this.state.checked.some((item)=>{
                if(!item){
                    alert('Checklist Belum Lengkap');
                    return true;
                }
                return false;
            });
            if(check){
                this.setState({
                    saving:false,
                    modalRating: false
                });
                return false;
            }    
        }
        this.setState({modalRating:true});
    }

    beriRating(){
        return (
            <Card transparent>
                <CardItem style={{flexDirection:'row'}}>        
                    <TouchableOpacity 
                        transparent  
                        onPress={() => this.setState({rating:1})}>
                        <View style={[containerStyle, (this.state.rating==1) ? {backgroundColor:'green'} : {}]}>
                            <Image style={imageStyle} source={smileImage} />
                        </View>
                        <Text style={{textAlign:'center', marginRight:20}}>Puaaas</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        transparent 
                        onPress={() => this.setState({rating:0})}>
                        <View style={[containerStyle, (this.state.rating==0) ? {backgroundColor:'#ff0777'} : {}]}>
                            <Image style={imageStyle} source={sadImage} />
                        </View>
                        <Text style={{textAlign:'center', marginRight:20}}>Tidak Puas</Text>
                    </TouchableOpacity>
                </CardItem>
                <CardItem style={{flexDirection:'row'}}>
                    <Textarea onChangeText={(text) => this.setState({komentar: text})} width='100%' rowSpan={2} bordered placeholder='Komentar' />
                </CardItem>
                <CardItem style={{flexDirection:'row'}}>
                    <Textarea onChangeText={(text) => this.setState({preventive: text})} width='100%' rowSpan={2} bordered placeholder='Keterangan Preventive' />
                </CardItem>
                <CardItem style={{flexDirection:'row'}}>
                    <Textarea onChangeText={(text) => this.setState({corrective: text})} width='100%' rowSpan={2} bordered placeholder='Keterangan Corrective' />
                </CardItem>
                <CardItem style={{flexDirection:'row'}}>
                    <Button onPress={() => this.save()}><Text>Simpan</Text></Button>
                </CardItem>
            </Card>  
        )
    }

    render(){
        return(
            <Container>
            <Header>
              <Left>
                <Button 
                  transparent 
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Icon name="arrow-back" />
                </Button>
              </Left>
              <Body style={{ flex: 1 }}>
                <Title> Check List Kunjungan</Title>
              </Body>
              <Right>
                <Button 
                  transparent 
                  onPress={() => this.showRating()}
                >
                  <Text>Simpan</Text>
                  <Icon name="ios-send" />
                </Button>

              </Right>
            </Header>
            <Content>
                <Modal
                    style={modalContentStyle} 
                    isVisible={this.state.modalRating} 
                    onBackdropPress={() => this.setState({modalRating:false})}
                    onBackButtonPress={() => this.setState({modalRating:false})}>
                    <View style={{flex:1}}>{this.beriRating()}</View>
                </Modal>
                <List>
                    {this.showSpinner()}
                    {this.getCheckList()}
                </List>
            </Content>
            </Container>
        )
    }
}