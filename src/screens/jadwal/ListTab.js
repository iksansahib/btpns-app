import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  Text,
  Right,
  Left,
  Body,
  TabHeading,
  DatePicker,
  Content
} from "native-base";
import ListPending from "./ListPending";
import ListSelesai from "./ListSelesai";

class ListTab extends Component {
  constructor(){
    super();
    this.state = {
      new_date: null
    };
  }
  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button 
              transparent 
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title> List Jadwal</Title>
          </Body>
          <Right />
        </Header>
        <Tabs swipeEnabled={false} style={{ elevation: 3 }}>
          <Tab
            heading={
              <TabHeading>
                <Text>Jadwal</Text>
              </TabHeading>
            }
          >
            <DatePicker
              defaultDate={new Date()}
              locale={'en'}
              onDateChange={(date) => {
                  this.setState({new_date:date.toLocaleDateString('en-US', { timezone: 'Asia/Jakarta'})});
                } 
              } 
            />
            <ListPending new_date={this.state.new_date} navigation={this.props.navigation} />
          </Tab>
          <Tab
            heading={
              <TabHeading>
                <Text>Actual</Text>
              </TabHeading>
            }
          >
            <ListSelesai navigation={this.props.navigation} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
export default ListTab;
