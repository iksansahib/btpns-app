import React, { Component } from "react";
import { 
    Content, 
    Text, 
    Card, 
    CardItem, 
    Left, 
    Body, 
    View, 
    Button, 
    Container, 
    List, 
    Spinner,
    Right,
    Icon,
    ListItem
} from "native-base";
import axios from 'axios';
import { FlatList, AsyncStorage } from 'react-native';
import ListOrder from '../list/ListOrder';
import Modal from "react-native-modal";
import { url_api } from '../../config';

const modalContentStyle = {
  backgroundColor: "white",
  padding: 22,
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 4,
  borderColor: "rgba(0, 0, 0, 0.1)"
}
export default class ListSelesai extends Component {
  constructor(){
    super();
    this.state = {
      orders : [],
      loading: false,
      hideSpinner: true,
      page: 0,
      loadMoreLoading: false,
      refresh: false,
      waitBeforeRefresh: 0,
      ft_id: null,
      m_user_id: null
    };
  }
  componentDidMount(){
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      this.refreshItem();
    });
    this.refreshItem();
}

  async loadMore(){
    const page = this.state.page+1;
    this.setState({page: page, loadMoreLoading:true});
    axios.get(`${url_api}/api/jadwaldone/${this.state.ft_id}/${page}/20?status=1`)
      .then((res) => {
        this.setState({ 
          orders: [...this.state.orders, ...res.data], 
          loadMoreLoading:false 
        })
      }).catch((err) => {
        console.log(err);
        this.setState({loadMoreLoading:false});
      });
  }

  async refreshItem(){
    const page = 0;
    const ft_id = await AsyncStorage.getItem('ft_id');
    const m_user_id = await AsyncStorage.getItem('m_user_id');
    this.setState({ft_id});
    this.setState({m_user_id});
    this.setState({page: page, loading:true});
    second = Math.round(new Date().getTime()/1000);
    if((this.state.waitBeforeRefresh==0) || ((second-this.state.waitBeforeRefresh)>3)){
      axios.get(`${url_api}/api/jadwaldone/${this.state.ft_id}/${page}/20?status=1`)
      .then((res) => {
        second = Math.round(new Date().getTime()/1000);
        console.log('refresh one');
        this.setState({ orders: res.data, loading:false, waitBeforeRefresh: second});
      }).catch((err) => {
        this.setState({loading:false});
      });
    } else {
      this.setState({loading:false});
    }
  }
  runEksekusi(m_jadwal_id){
    arr = [...this.state.orders];
    arr.splice(m_jadwal_id, 1);
    this.setState({orders:arr});
  }

  buttonRefresh(){
    if(this.state.orders.length==0){
      return (
        <Button 
          style={{alignSelf:'center', marginTop:10}}
          onPress={() => this.refreshItem()}>
          <Text>Refresh</Text>
        </Button>
      )
    } 
  }
  renderOrders(){
    if(!this.state.loading){
      return ( 
        <FlatList 
          data={this.state.orders} 
          renderItem={({item}) => {
            return (
                <List>
                    <ListJadwal navigation={this.props.navigation} key={item.m_jadwal_id} order={item} />
                </List>
            )
          }}
          keyExtractor={(item,index) => item.m_jadwal_id.toString()}
          onEndReached={this.loadMore.bind(this)}
          onRefresh={this.refreshItem.bind(this)}
          refreshing={this.state.loading}
          onEndReachedThreshold={0.1}
          extraData={this.state.refresh}
          />
      );
    } else {
      return (<Spinner />)
    }
  }
  renderLoadMoreLoading(){
    if(this.state.loadMoreLoading){
      return <Spinner />;
    }
  }
  render() {
    return (
      <List>
        {this.buttonRefresh()}
        {this.renderOrders()}
        {this.renderLoadMoreLoading()}
      </List>
    );  
  }
}

const jenis = { 
  '66': 'Wisma',
  '67': 'Motor'
};

class ListJadwal extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <ListItem bordered key={this.props.order.m_jadwal_id}>
              <Body>
                  <Text>{this.props.order.wisma.name}</Text>
                  <Text note>{jenis[this.props.order.kategori_id]}</Text>
                  <Text note>Jadwal: {this.props.order.jadwal}</Text>
                  <Text note>Selesai: {this.props.order.check_listed}</Text>
              </Body>
            </ListItem>
        )
    }
}
