import React, { Component } from 'react';
import { View, Text, Container, Header, Left, Button, Icon, Body, Title, Right, Content, Fab, Footer, Card, CardItem } from 'native-base';
import Axios from 'axios';
import { url_api } from '../../config';
import ListUnitPrice from '../list/ListUnitPrice';
import { Alert } from 'react-native';
import Modal from 'react-native-modal';

const modalContentStyle = {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
}
 
class UnitPrice extends Component{
    constructor(props){
        super(props);
        this.state = {
            t_order_id: 0,
            unitprice: [],
            viewform: false,
            loading:false
        };
    }

    listUnitPrice(){
        return (
            this.state.unitprice.map((unitprice) => {
                const unit_price_name = (unitprice.unitprice!=null) 
                    ? unitprice.unitprice.name
                    : `${unitprice.unit_price} (Rp.${unitprice.harga_unit_price})`
                return(
                    <CardItem bordered key={unitprice.t_order_unit_price_id}>
                        <Body>
                            <Text>{unit_price_name}</Text>
                        </Body>
                        <Right>
                            <Button 
                                transparent 
                                onPress={() => this.confirmHapus(unitprice.t_order_unit_price_id)}>
                                <Text>Hapus</Text>
                            </Button>
                        </Right>
                    </CardItem>
                )
            })
        )
    }
    componentDidMount(){
        const { navigation } = this.props;
        const id = navigation.getParam('id');
        this.setState({t_order_id:  id});
        this.getUnitPrice(id);
    }

    getUnitPrice(id){
        this.setState({loading:true});
        Axios.get(url_api+'/api/unitpricebyorder/'+id)
            .then((res)=> {
                this.setState({ unitprice: res.data, loading:false });
            }).catch(err => this.setState({loading:false}));
    }
    removeUnitPrice(id){
        Axios.delete(url_api+'/api/orderunitprice/'+id)
            .then((req,res) => {
                alert('Berhasil Dihapus');
                this.getUnitPrice(this.state.t_order_id);
            });
    }
    confirmHapus(id){
        Alert.alert(
            'Konfirmasi Hapus',
            'Hapus?',
            [
                {text:'OK', onPress: () => this.removeUnitPrice(id)},
                {text:'Batal'}
            ]
        );
    }

    render(){
        return(
            <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                    <Icon name="arrow-back" />
                    </Button>
                </Left>
                <Body>
                    <Title>Unit Price</Title>
                </Body>
                <Right>
                    <Button onPress={() => this.props.navigation.navigate('OrderKategori')} transparent iconRight>
                        <Text>SIMPAN</Text>
                        <Icon name="ios-send" />
                    </Button>
                </Right>
            </Header>
            <Content contentContainerStyle={{flex:1}}>
                <Modal isVisible={this.state.loading}>
                    <View style={modalContentStyle}>
                        <Text>Loading...</Text>
                    </View>
                </Modal>
                <Card>
                    {this.listUnitPrice()}
                </Card>
                <Fab
                    active={true}
                    position='bottomRight'
                    direction='up'
                    style={{backgroundColor:'#156ac5'}}
                    onPress={()=>this.props.navigation.navigate('UnitPriceForm',{
                        id: this.state.t_order_id
                    })}>
                    <Icon name='ios-add' />
                </Fab>
            </Content>
            </Container>            
        )
    }
}

export default UnitPrice;