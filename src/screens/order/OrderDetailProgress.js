import React, { Component } from 'react';
import { 
    Card, 
    CardItem, 
    Text, 
    Content, 
    Container, 
    Header, 
    Left, 
    Button, 
    Body, 
    Right,
    Icon,
    Title,
    View,
    Tabs,
    Tab,
    TabHeading,
    Textarea,
    Label
} from "native-base";
import { url_api } from '../../config';
import Axios from 'axios';
import OrderView from './OrderView';
import OrderUnitPrice from './OrderUnitPrice';
import { Image, Alert, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
const containerStyle = {
    borderRadius:10, 
    overflow:'hidden', 
    height:100, 
    width:100, 
    alignItems:'center',
    alignSelf: 'stretch', 
    justifyContent:'center',
    marginRight:20
};

const modalContentStyle = {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
 }

const smileImage = require('../../../assets/smile.png');
const sadImage = require('../../../assets/sad.png');

const imageStyle = {
    overflow:'hidden', 
    height:80, 
    width:80, 
};
 
class OrderDetailProgress extends Component
{
    constructor(){
        super();
        this.state = {
            t_order_id: null,
            saving: false,
            textLoading: 'SELESAI',
            modalRating: false,
            rating: null,
            komentar: ''
        }
    }
    componentWillMount(){
        const { navigation } = this.props;
        const t_order_id = navigation.getParam('t_order_id');

        this.setState({
            t_order_id: t_order_id
        });
    }

    approve(){
        this.setState({
            saving:true,
            textLoading: 'LOADING'
        });
        let form = new FormData();
        form.append('puas', this.state.rating);
        form.append('komentar', this.state.komentar);
        form.append('t_order_id', this.state.t_order_id);

        Axios.post(
            `${url_api}/api/orderrating`,
            form, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept':'application/json'
                }          
            }
        ).then(resp => {
            this.setState({
                saving:false,
                textLoading: 'SELESAI'
            });
            Alert.alert('Sukses', 
                resp.data.msg,
                [{text: 'OK', onPress: (() => this.props.navigation.goBack())}]
            );
        }).catch(err => {
            this.setState({
                saving:false,
                textLoading: 'SELESAI'
            });
            alert(err);
        });

    }
    beriRating(){
        return (
            <Card transparent>
                <CardItem style={{flexDirection:'row'}}>        
                    <TouchableOpacity 
                        transparent  
                        onPress={() => this.setState({rating:1})}>
                        <View style={[containerStyle, (this.state.rating==1) ? {backgroundColor:'green'} : {}]}>
                            <Image style={imageStyle} source={smileImage} />
                        </View>
                        <Text style={{textAlign:'center', marginRight:20}}>Puaaas</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        transparent 
                        onPress={() => this.setState({rating:0})}>
                        <View style={[containerStyle, (this.state.rating==0) ? {backgroundColor:'#ff0777'} : {}]}>
                            <Image style={imageStyle} source={sadImage} />
                        </View>
                        <Text style={{textAlign:'center', marginRight:20}}>Tidak Puas</Text>
                    </TouchableOpacity>
                </CardItem>
                <CardItem style={{flexDirection:'row'}}>
                    <Textarea onChangeText={(text) => this.setState({komentar: text})} width='100%' rowSpan={5} bordered placeholder='Komentar' />
                </CardItem>
                <CardItem style={{flexDirection:'row'}}>
                    <Button onPress={() => this.approve()}><Text>Simpan</Text></Button>
                </CardItem>
            </Card>  
        )
    }
    render(){
        return (
            <Container>
            <Header hasTabs>
              <Left>
                <Button 
                  transparent 
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Icon name="arrow-back" />
                </Button>
              </Left>
              <Body style={{ flex: 1 }}>
                <Title> Detail Order</Title>
              </Body>
              <Right>
                <Button 
                  disabled={this.state.saving}
                  transparent 
                  onPress={() => this.setState({modalRating:true})}
                >
                  <Text>{this.state.textLoading}</Text>
                  <Icon name="ios-send" />
                </Button>

              </Right>
            </Header>
            <View>
                <Modal
                    style={modalContentStyle} 
                    isVisible={this.state.modalRating} 
                    onBackdropPress={() => this.setState({modalRating:false})}>
                    <View style={{flex:1}}>{this.beriRating()}</View>
                </Modal>
            </View>
            <Tabs swipeEnabled={false} style={{ elevation: 3 }}>
                <Tab
                    heading={
                    <TabHeading>
                        <Text>Order</Text>
                    </TabHeading>
                    }
                >
                    <OrderView t_order_id={this.state.t_order_id} />
                </Tab>
                <Tab
                    heading={
                    <TabHeading>
                        <Text>Unit Price</Text>
                    </TabHeading>
                    }
                >
                    <OrderUnitPrice t_order_id={this.state.t_order_id} />
                </Tab>
            </Tabs>
            </Container>
        )
    }
}

export default OrderDetailProgress;