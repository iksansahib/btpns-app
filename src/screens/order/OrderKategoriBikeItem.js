import React, { Component } from "react";
import { View, Text, Container, Header, Left, Button, Icon, Body, Title, Content, Right } from "native-base";
import styles from "./styles";
import { Image, TouchableOpacity } from 'react-native';

const containerStyle = {
  borderRadius:10, 
  overflow:'hidden', 
  height:100, 
  width:100, 
  alignItems:'center',
  alignSelf: 'stretch', 
  justifyContent:'center',
  marginRight:20
};

const textStyle = {
  textAlign:'center', 
  marginRight:20
};

const imageStyle = {
  overflow:'hidden', 
  height:80, 
  width:80, 
};

const waterColor = {
  backgroundColor:'dodgerblue', 
};

const listrikColor = {
  backgroundColor:'#0FBC66', 
};

const pengamanColor = {
  backgroundColor:'#e05a26', 
};

const banColor = {
    backgroundColor:'#b22222', 
};

const aksesorisColor = {
backgroundColor:'#7fff00', 
};
  
const mesinImage = require('../../../assets/mesin.png');
const oliImage = require('../../../assets/oli.png');
const listrikImage = require('../../../assets/electric.png');
const banImage = require('../../../assets/ban.png');
const aksesorisImage = require('../../../assets/aksesoris.png');

export default class OrderKategoriBikeItem extends Component {
    render() {
      return (
        <Container style={styles.container}>
          <Header>
            <Left>
              <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>FAS - BTPNS</Title>
            </Body>
            <Right />
          </Header>
  
          <Content padder>
            <View style={{flexDirection:'row'}}>        
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderFormBike', {
                    kategori_id: 67,
                    kategori_item_perbaikan_id: 82,
                    sub_kategori_item_perbaikan_id: 0
                })}>
                    <View style={[containerStyle, waterColor]}>
                        <Image style={imageStyle} source={mesinImage} />
                    </View>
                    <Text style={textStyle}>Mesin</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderFormBike', {
                    kategori_id: 67,
                    kategori_item_perbaikan_id: 201,
                    sub_kategori_item_perbaikan_id: 0
                })}>
                    <View style={[containerStyle, listrikColor]}>
                        <Image style={imageStyle} source={oliImage} />
                    </View>
                    <Text style={textStyle}>Oli</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderFormBike', {
                    kategori_id: 67,
                    kategori_item_perbaikan_id: 198,
                    sub_kategori_item_perbaikan_id: 79
                })}>
                    <View style={[containerStyle, pengamanColor]}>
                        <Image style={imageStyle} source={listrikImage} />
                    </View>
                    <Text style={textStyle}>Kelistrikan</Text>
                </TouchableOpacity>

            </View>
            <View style={{flexDirection:'row'}}>        
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderFormBike', {
                    kategori_id: 67,
                    kategori_item_perbaikan_id: 203,
                    sub_kategori_item_perbaikan_id: 0
                })}>
                    <View style={[containerStyle, banColor]}>
                        <Image style={imageStyle} source={banImage} />
                    </View>
                    <Text style={textStyle}>Ban</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderFormBike', {
                    kategori_id: 67,
                    kategori_item_perbaikan_id: 205,
                    sub_kategori_item_perbaikan_id: 0
                })}>
                    <View style={[containerStyle, aksesorisColor]}>
                        <Image style={imageStyle} source={aksesorisImage} />
                    </View>
                    <Text style={textStyle}>Aksesoris</Text>
                </TouchableOpacity>
            </View>
          </Content>
        </Container>
      );
    }
}