import { View } from 'native-base';
import React from 'react';
import { View } from 'native-base';
import { Image, TouchableOpacity } from 'react-native';

export const OrderOptionBikeItem = (props) => {
    const wheelImage = require('../../../assets/wheel.png');
    const containerStyle = {
        borderRadius:10, 
        overflow:'hidden', 
        backgroundColor:'cadetblue', 
        height:100, 
        width:100, 
        alignItems:'center',
        alignSelf: 'stretch', 
        justifyContent:'center',
        marginRight:20
    };

    const imageStyle = {
        overflow:'hidden', 
        height:80, 
        width:80, 
    };
    return (
        <View style={{flexDirection:'row'}}>        
            <TouchableOpacity onPress={() => props.wheel}>
                <View style={containerStyle}>
                    <Image style={imageStyle} source={wheelImage} />
                </View>
            </TouchableOpacity>
        </View>
    )
};