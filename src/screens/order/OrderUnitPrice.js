import React, { Component } from 'react';
import { View, Text, Container, Body, Content, Card, CardItem } from 'native-base';
import Axios from 'axios';
import { url_api } from '../../config';

 
class OrderUnitPrice extends Component{
    constructor(props){
        super(props);
        this.state = {
            t_order_id: 0,
            unitprice: [],
            viewform: false,
            loading:false
        };
    }

    listUnitPrice(){
        return (
            this.state.unitprice.map((unitprice) => {
                const unit_price = (unitprice.unitprice!=null)
                    ? unitprice.unitprice.name
                    : `${unitprice.unit_price} (Rp.${unitprice.harga_unit_price})`
                return(
                    <CardItem bordered key={unitprice.t_order_unit_price_id}>
                        <Body>
                            <Text>{unit_price}</Text>
                        </Body>
                    </CardItem>
                )
            })
        )
    }
    componentDidMount(){
        const { t_order_id } = this.props;
        const id = t_order_id;
        this.setState({t_order_id:  id});
        this.getUnitPrice(id);
    }

    getUnitPrice(id){
        this.setState({loading:true});
        Axios.get(url_api+'/api/unitpricebyorder/'+id)
            .then((res)=> {
                console.log(res);
                this.setState({ unitprice: res.data, loading:false });
            }).catch(err => this.setState({loading:false}));
    }

    render(){
        return(
            <Container>
            <Content contentContainerStyle={{flex:1}}>
                <Card>
                    {this.listUnitPrice()}
                </Card>
            </Content>
            </Container>            
        )
    }
}

export default OrderUnitPrice;