import React, { Component } from 'react';
import { 
    Header, 
    Left, 
    Icon, 
    Body, 
    Right, 
    Container, 
    Text, 
    Button,
    Title,
    Content,
    View,
    FooterTab,
    Footer,
    Form,
    Textarea,
    Item,
    Label,
    Picker
} from "native-base";

import { Image, Alert, AsyncStorage } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Axios from 'axios';
import Modal from 'react-native-modal';
import { url_api } from '../../config';
const opt = {
    title: 'Upload Foto'
}

const imageStyle = {
    height:150, 
    width:150, 
    left:0,
    resizeMode:'cover',
    top:0
};

const trashStyle = {
    borderRadius:50,
    top:7, 
    right:0, 
    left:7,
    bottom:0,
    position:'absolute',
    width:50
};

const modalContentStyle = {
   backgroundColor: "white",
   padding: 22,
   justifyContent: "center",
   alignItems: "center",
   borderRadius: 4,
   borderColor: "rgba(0, 0, 0, 0.1)"
}
export default class OrderFormBike extends Component {
    constructor(){
        super();
        this.state = {
            image: [],
            deskripsi: '',
            postData : [],
            kategori_id: 0,
            kategori_item_perbaikan_id: 0,
            sub_kategori_item_perbaikan_id: 0,
            sub_kategori_item_perbaikan_list: [],
            saving: false,
            selectedWisma: null,
            selectedSubKategori: null,
            m_user_id: null,
            ft_id: null,
            wisma: []
        };
    }

    async componentDidMount(){
        const m_user_id = await AsyncStorage.getItem('m_user_id');
        const ft_id = await AsyncStorage.getItem('ft_id');
        const { navigation } = this.props;
        const kategori_id = navigation.getParam('kategori_id');
        const kategori_item_perbaikan_id = navigation.getParam('kategori_item_perbaikan_id');
        const sub_kategori_item_perbaikan_id = navigation.getParam('sub_kategori_item_perbaikan_id');
        this.setState({
            kategori_id: kategori_id,
            kategori_item_perbaikan_id: kategori_item_perbaikan_id,
            sub_kategori_item_perbaikan_id: sub_kategori_item_perbaikan_id,
            m_user_id: m_user_id,
            ft_id: ft_id
        });

        Axios.get(url_api+`/api/sub_kategori/${kategori_item_perbaikan_id}`)
            .then((res)=>{
                this.setState({sub_kategori_item_perbaikan_list:res.data});
            });

        Axios.get(url_api+`/api/wisma/${ft_id}`)
            .then((res)=>{
                this.setState({wisma:res.data});
            });

    }

    imagePicker(){
        ImagePicker.launchImageLibrary(opt, (resp)=>{
            this.handleImage(resp);
        });
    }

    photoPicker(){
        ImagePicker.launchCamera(opt, (resp)=>{
            this.handleImage(resp);
        });
    }

    handleImage(resp){
        if(!resp.didCancel 
            && !resp.customButton 
            && !resp.error){
            
            if(this.state.image.length==0){
                newImageArray = [];
            } else {
                newImageArray = this.state.image.slice();
            }
            newImageArray.push({uri: resp.uri});
            this.setState({ image: newImageArray });
        }
    }

    removeImage(image){
        array = [...this.state.image];
        removed = array.indexOf(image);
        array.splice(removed, 1);
        this.setState({image:array})
    }
    confirmRemove(image){
        Alert.alert(
            'Konfirmasi Hapus',
            'Hapus?',
            [
                {text:'OK', onPress: () => this.removeImage(image)},
                {text:'Batal'}
            ]
        );
    }
    renderImage(){
        let i = 0;
        return this.state.image.map(image => {
            i++;
            return (
                <View key={i} style={{width:150, height:null}}>
                    <Image source={image} style={imageStyle} />
                    <Button onPress={() => this.confirmRemove(image)} danger style={trashStyle} icon><Icon color='#ff00' name='trash' /></Button>
                </View>
            )
        });
    }
    saveOrder(){
        if(this.state.selectedWisma===0){
            Alert.alert('Error', 
                'Wisma Tidak Boleh Kosong',
                [{text: 'OK'}]
            );
            return false;
        }

        if(this.state.selectedSubKategori===0){
            Alert.alert('Error', 
                'Sub Kategori Tidak Boleh Kosong',
                [{text: 'OK'}]
            );
            return false;
        }

        this.setState({saving:true});
        let form = new FormData();

        form.append('kategori_id', this.state.kategori_id);
        form.append('kategori_item_perbaikan_id',this.state.kategori_item_perbaikan_id);
        form.append('sub_kategori_item_perbaikan_id',this.state.selectedSubKategori);
        form.append('m_wisma_id',this.state.selectedWisma);
        form.append('m_user_id',this.state.m_user_id);
        form.append('deskripsi',this.state.deskripsi);

        this.state.image.map((image,i) => form.append('image[]', {
            uri: image.uri,
            type: 'image/jpeg',
            name: 'Image'+i
        }));
        Axios.post(
            `${url_api}/api/order`,
            form, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept':'application/json'
                }          
            }
        ).then(resp => {
            this.setState({saving:false});
            id = resp.data.id;
            console.log(id);
            Alert.alert('Sukses', 
                'Deskripsi dan Image Berhasil Disimpan',
                [{text: 'OK', onPress: (() => this.props.navigation.navigate('UnitPrice',{
                    id: id
                }))}]
            );
        }).catch(err => {
            this.setState({saving:false});
            alert(err);
        });
    }

    onWismaValueChange(value){
        this.setState({
            selectedWisma: value
        })
    }

    onSubKategoriValueChange(value){
        this.setState({
            selectedSubKategori: value
        })
    }

    wismaList(){
        return this.state.wisma.map((wisma)=> 
            <Picker.Item key={wisma.m_wisma_id} label={`${wisma.code} - ${wisma.name}`} value={wisma.m_wisma_id} />
        )
    }

    subKategoriList(){
        return this.state.sub_kategori_item_perbaikan_list.map((kategori_list)=> 
            <Picker.Item key={kategori_list.m_reference_id} label={`${kategori_list.name}`} value={kategori_list.m_reference_id} />
        )
    }

    render(){
        return(
            <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                    <Icon name="arrow-back" />
                    </Button>
                </Left>
                <Body>
                    <Title>FAS - BTPNS</Title>
                </Body>
                <Right>
                    <Button onPress={() => this.saveOrder()} transparent iconRight>
                        <Text>UNIT PRICE</Text>
                        <Icon name="arrow-forward" />
                    </Button>
                </Right>
            </Header>
    
            <Content>
                {/* <View style={{flex:1,flexDirection:'row', flexWrap:'wrap'}}>         */}
                <Content padder>
                    <Modal isVisible={this.state.saving}>
                        <View style={modalContentStyle}>
                            <Text>Sedang Menyimpan Data...</Text>
                        </View>
                    </Modal>
                    <Form>
                        <Item>
                            <Picker
                                inlineLabel='Wisma'
                                mode='dropdown'
                                placeholder='Input Wisma'
                                selectedValue={this.state.selectedWisma}
                                onValueChange={this.onWismaValueChange.bind(this)}>
                                <Picker.Item label='Pilih Wisma' value={0}></Picker.Item>
                                {this.wismaList()}
                            </Picker>
                        </Item>
                        <Item>
                            <Picker
                                inlineLabel='Sub Kategori'
                                mode='dropdown'
                                placeholder='Input Sub Kategori'
                                selectedValue={this.state.selectedSubKategori}
                                onValueChange={this.onSubKategoriValueChange.bind(this)}>
                                <Picker.Item label='Pilih Sub Kategori' value={0}></Picker.Item>
                                {this.subKategoriList()}
                            </Picker>
                        </Item>
                        <Item stackedLabel>
                        <Label>Deskripsi</Label>
                        <Textarea onChangeText={(text) => this.setState({deskripsi: text})} width='100%' rowSpan={5} bordered placeholder='Deskripsi' />
                        </Item>
                    </Form>
                </Content>
                {/* </View> */}
                <View style={{flexDirection:'row', flexWrap:'wrap'}}>        
                    {this.renderImage()}
                </View>
            </Content>
            <Footer>
                <FooterTab>
                    <Button
                        onPress={() => {this.imagePicker()}}
                        icon light>
                        <Icon name='ios-images' />
                        <Text>Galeri</Text>
                    </Button>
                </FooterTab>
                <FooterTab>
                    <Button
                        onPress={() => {this.photoPicker()}}
                        icon light>
                        <Icon name='ios-camera' />
                        <Text>Camera</Text>
                    </Button>
                </FooterTab>
            </Footer>
          </Container>
        )
    }
}