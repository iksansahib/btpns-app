import React, { Component } from 'react';
import { 
    Card, 
    CardItem, 
    Text, 
    Content, 
    Body, 
    View
} from "native-base";
import Modal from 'react-native-modal';
import { url_api } from '../../config';
import axios from 'axios';
import { Image } from 'react-native';

const modalContentStyle = {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
}

export default class OrderView extends Component
{
    constructor(props){
        super(props);
        this.state = {
            t_order_id: null,
            orders: [],
            loading: true,
            wisma_name: "",
            kategori_item_name: "",
            sub_kategori_item_name: "",
            deskripsi: "",
            notes: "",
            image: []
        }
    }
    componentDidMount(){
        this.setState({loading:true});
        axios.get(url_api+'/api/order/'+this.props.t_order_id)
            .then((res) => {
                approval = res.data.approval;
                notes = "";
                approval.map((item)=>{
                    if(item.notes!=null){
                        if(notes!=""){
                            notes += "\n\n\n";
                        }
                        notes += item.notes;
                    }
                });
                this.setState({ 
                    orders: res.data,
                    wisma_name: res.data.wisma.name,
                    status_name: res.data.status.name,
                    kategori_item_name: res.data.kategori_item.name,
                    image: res.data.image,
                    sub_kategori_item_name: res.data.sub_kategori_item.name,
                    deskripsi: res.data.deskripsi,
                    notes: notes,
                    loading:false
                    });
            }).catch((err) => {
                this.setState({loading:false});
                console.log(err);
            });
    }
    renderImage(){
        return this.state.image.map((image) => {
            const image_path = url_api + image.image.replace("public/foto","/storage/foto");
            return (
                <CardItem key={image.t_order_image_id}>
                    <Image style={{width:300, height:300}} key={image.t_order_image_id} source={{uri: image_path}} />
                </CardItem>
            )
        })
    }
    render(){
        return (
            <Content>
                <Modal isVisible={this.state.loading}>
                    <View style={modalContentStyle}>
                        <Text>Loading Data...</Text>
                    </View>
                </Modal>
                <Card>
                    <CardItem style={{flexDirection:'column'}}>
                        <Body style={{marginTop:5}}>
                            <Text>Wisma</Text>
                            <Text note>{this.state.wisma_name}</Text>
                        </Body>
                        <Body style={{marginTop:5}}>
                            <Text>Perbaikan</Text>
                            <Text note>{this.state.kategori_item_name}</Text>
                        </Body>
                        <Body style={{marginTop:5}}>
                            <Text>Sub Kategori Perbaikan</Text>
                            <Text note>{this.state.sub_kategori_item_name}</Text>
                        </Body>
                        <Body style={{marginTop:5}}>
                            <Text>Status</Text>
                            <Text note>{this.state.status_name}</Text>
                        </Body>
                        <Body style={{marginTop:5}}>
                            <Text>Deskripsi</Text>
                            <Text note>{this.state.deskripsi}</Text>
                        </Body>
                        <Body style={{marginTop:5}}>
                            <Text>Notes</Text>
                            <Text note>{this.state.notes}</Text>
                        </Body>
                    </CardItem>
                    {this.renderImage()}
                </Card>
            </Content>
        )
    }
}
