import React, { Component } from "react";
import { View, Text, Container, Header, Left, Button, Icon, Body, Title, Content, Right } from "native-base";
import styles from "./styles";
import { Image, TouchableOpacity } from 'react-native';

const containerStyle = {
  borderRadius:10, 
  overflow:'hidden', 
  height:100, 
  width:100, 
  alignItems:'center',
  alignSelf: 'stretch', 
  justifyContent:'center',
  marginRight:20
};

const textStyle = {
  textAlign:'center', 
  marginRight:20
};

const imageStyle = {
  overflow:'hidden', 
  height:80, 
  width:80, 
};

const waterColor = {
  backgroundColor:'dodgerblue', 
};

const listrikColor = {
  backgroundColor:'#0FBC66', 
};

const pengamanColor = {
  backgroundColor:'#e05a26', 
};

const waterImage = require('../../../assets/water.png');
const listrikImage = require('../../../assets/listrik.png');
const gembokImage = require('../../../assets/gembok.png');

export default class OrderKategoriHomeItem extends Component {
    render() {
      return (
        <Container style={styles.container}>
          <Header>
            <Left>
              <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>FAS - BTPNS</Title>
            </Body>
            <Right />
          </Header>
  
          <Content padder>
            <View style={{flexDirection:'row'}}>        
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderSubKategoriAir')}>
                    <View style={[containerStyle, waterColor]}>
                        <Image style={imageStyle} source={waterImage} />
                    </View>
                    <Text style={textStyle}>Air</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderSubKategoriListrik')}>
                    <View style={[containerStyle, listrikColor]}>
                        <Image style={imageStyle} source={listrikImage} />
                    </View>
                    <Text style={textStyle}>Listrik</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderSubKategoriPengaman')}>
                    <View style={[containerStyle, pengamanColor]}>
                        <Image style={imageStyle} source={gembokImage} />
                    </View>
                    <Text style={textStyle}>Pengaman</Text>
                </TouchableOpacity>
            </View>
          </Content>
        </Container>
      );
    }
}