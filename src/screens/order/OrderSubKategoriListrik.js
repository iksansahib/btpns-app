import React, { Component } from "react";
import { View, Text, Container, Header, Left, Button, Icon, Body, Title, Content, Right } from "native-base";
import styles from "./styles";
import { Image, TouchableOpacity } from 'react-native';

const containerStyle = {
  borderRadius:10, 
  overflow:'hidden', 
  height:100, 
  width:100, 
  alignItems:'center',
  alignSelf: 'stretch', 
  justifyContent:'center',
  marginRight:20
};

const textStyle = {
  textAlign:'center', 
  marginRight:20
};

const imageStyle = {
  overflow:'hidden', 
  height:80, 
  width:80, 
};

const saklarColor = {
  backgroundColor:'dodgerblue', 
};

const lampuColor = {
  backgroundColor:'#0FBC66', 
};

const kabelColor = {
  backgroundColor:'#b22222', 
};

const saklarImage = require('../../../assets/saklar.png');
const lampuImage = require('../../../assets/listrik.png');
const kabelImage = require('../../../assets/kabel.png');

export default class OrderSubKategoriListrik extends Component {
    render() {
      return (
        <Container style={styles.container}>
          <Header>
            <Left>
              <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>FAS - BTPNS</Title>
            </Body>
            <Right />
          </Header>
  
          <Content padder>
            <View style={{flexDirection:'row'}}>        
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('OrderForm', {
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 69,
                        sub_kategori_item_perbaikan_id: 76
                    })}>
                    <View style={[containerStyle, saklarColor]}>
                        <Image style={imageStyle} source={saklarImage} />
                    </View>
                    <Text style={textStyle}>Stop Kontak</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderForm', {
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 69,
                        sub_kategori_item_perbaikan_id: 77
                    })}>
                    <View style={[containerStyle, lampuColor]}>
                        <Image style={imageStyle} source={lampuImage} />
                    </View>
                    <Text style={textStyle}>Lampu</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderForm', {
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 69,
                        sub_kategori_item_perbaikan_id: 268
                    })}>
                    <View style={[containerStyle, kabelColor]}>
                        <Image style={imageStyle} source={kabelImage} />
                    </View>
                    <Text style={textStyle}>Kabel</Text>
                </TouchableOpacity>
            </View>
          </Content>
        </Container>
      );
    }
}