import React, { Component } from 'react';
import { 
    Card, 
    CardItem, 
    Text, 
    Content, 
    Container, 
    Header, 
    Left, 
    Button, 
    Body, 
    Right,
    Icon,
    Title,
    View,
    Tabs,
    Tab,
    TabHeading
} from "native-base";
import { url_api } from '../../config';
import Axios from 'axios';
import OrderView from './OrderView';
import OrderUnitPrice from './OrderUnitPrice';
import { Alert } from 'react-native';

class OrderDetailDone extends Component
{
    constructor(){
        super();
        this.state = {
            t_order_id: null,
            saving: false,
            textLoading: 'SELESAI'
        }
    }
    componentWillMount(){
        const { navigation } = this.props;
        const t_order_id = navigation.getParam('t_order_id');

        this.setState({
            t_order_id: t_order_id
        });
    }

    approve(){
        this.setState({
            saving:true,
            textLoading: 'LOADING'
        });
        let form = new FormData();
        form.append('approve', '');
        form.append('t_order_id', this.state.t_order_id);
        form.append('current_status_id', '92');

        Axios.post(
            `${url_api}/api/approve`,
            form, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept':'application/json'
                }          
            }
        ).then(resp => {
            this.setState({
                saving:false,
                textLoading: 'SELESAI'
            });
            Alert.alert('Sukses', 
                resp.data.msg,
                [{text: 'OK', onPress: (() => this.props.navigation.goBack())}]
            );
        }).catch(err => {
            this.setState({
                saving:false,
                textLoading: 'SELESAI'
            });
            alert(err);
        });

    }
    render(){
        return (
            <Container>
            <Header hasTabs>
              <Left>
                <Button 
                  transparent 
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Icon name="arrow-back" />
                </Button>
              </Left>
              
              <Body style={{ flex: 1 }}>
                <Title> Detail Order</Title>
              </Body>
            </Header>
            <Tabs swipeEnabled={false} style={{ elevation: 3 }}>
                <Tab
                    heading={
                    <TabHeading>
                        <Text>Order</Text>
                    </TabHeading>
                    }
                >
                    <OrderView t_order_id={this.state.t_order_id} />
                </Tab>
                <Tab
                    heading={
                    <TabHeading>
                        <Text>Unit Price</Text>
                    </TabHeading>
                    }
                >
                    <OrderUnitPrice t_order_id={this.state.t_order_id} />
                </Tab>
            </Tabs>
            </Container>
        )
    }
}

export default OrderDetailDone;