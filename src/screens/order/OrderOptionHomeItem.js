import React from 'react';
import { View, Text } from 'native-base';
import { Image, TouchableOpacity } from 'react-native';

export const OrderOptionHomeItem = (props) => {
    const waterImage = require('../../../assets/water.png');
    const listrikImage = require('../../../assets/listrik.png');
    const gembokImage = require('../../../assets/gembok.png');
    const containerStyle = {
        borderRadius:10, 
        overflow:'hidden', 
        height:100, 
        width:100, 
        alignItems:'center',
        alignSelf: 'stretch', 
        justifyContent:'center',
        marginRight:20
    };

    const waterColor = {
        backgroundColor:'dodgerblue', 
    };

    const listrikColor = {
        backgroundColor:'#0FBC66', 
    };

    const pengamanColor = {
        backgroundColor:'#e05a26', 
    };
    const imageStyle = {
        overflow:'hidden', 
        height:80, 
        width:80, 
    };
};