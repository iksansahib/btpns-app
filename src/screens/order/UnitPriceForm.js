import React, { Component } from 'react';
import { View, Text, Container, Header, Left, Button, Icon, Body, Title, Right, Content, Fab, Footer, Picker, Form, Item, Input, Label, H3, Toast } from 'native-base';
import Axios from 'axios';
import { Alert } from 'react-native';
import { url_api } from '../../config';
 
class UnitPriceForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            t_order_id: 0,
            unitprice: [],
            viewform: false,
            selected: null,
            unit_price: '',
            harga_unit_price: 0
        };
    }
    componentDidMount(){
        const { navigation } = this.props;
        const id = navigation.getParam('id');
        this.setState({t_order_id: id});
        Axios.get(url_api+'/api/unitprice')
            .then((res)=>{
                this.setState({unitprice:res.data});
            })
    }
    onValueChange(value){
        this.setState({
            selected: value
        })
    }
    pickerList(){
        return this.state.unitprice.map((unitprice)=> 
            <Picker.Item key={unitprice.m_unit_price_id} label={unitprice.name} value={unitprice.m_unit_price_id} />
        )
    }
    saveUnitPrice(){
        if(this.state.selected!=0 
            || (this.state.unit_price!='' 
                && this.state.harga_unit_price!=0)){
            Axios.post(url_api+'/api/unitprice',{
                id: this.state.t_order_id,
                m_unit_price_id: this.state.selected,
                unit_price: this.state.unit_price,
                harga_unit_price : this.state.harga_unit_price
            }).then(res => {
                Alert.alert('Sukses', 
                    'Unit Price Berhasil Disimpan',
                    [{text: 'OK', onPress: (() => this.props.navigation.navigate('UnitPrice',{
                        id: this.state.t_order_id
                    }))}]
                );
            });    
        } else {
            Toast.show({
                'text': 'Pilih Unit Price atau Masukkan Unit Price',
                'type': 'danger',
                'buttonText': 'OK',
                'duration': 3000
            });
        }
    }

    render(){
        return(
            <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                    <Icon name="arrow-back" />
                    </Button>
                </Left>
                <Body>
                    <Title>Unit Price Form</Title>
                </Body>
                <Right>
                    <Button onPress={() => this.saveUnitPrice()} transparent iconRight>
                        <Text>SIMPAN</Text>
                        <Icon name="ios-send" />
                    </Button>
                </Right>
            </Header>
            <Content contentContainerStyle={{flex:1}}>
                <Form>
                    <Item>
                    <Picker
                        inlineLabel='Unit Price'
                        mode='dropdown'
                        placeholder='Input Unit Price'
                        selectedValue={this.state.selected}
                        onValueChange={this.onValueChange.bind(this)}>
                        <Picker.Item label='Pilih Unit Price' value={0}></Picker.Item>
                        {this.pickerList()}
                    </Picker>
                    </Item>
                    <Title>Lainnya</Title>
                    <Item>
                        <Input onChangeText={(unit_price) => this.setState({unit_price})} placeholder='Unit Price RAB' />
                    </Item>
                    <Item>
                        <Input onChangeText={(harga_unit_price)=>this.setState({harga_unit_price})} placeholder='Harga Unit Price RAB' />
                    </Item>
                </Form>
            </Content>
            </Container>            
        )
    }
}

export default UnitPriceForm;