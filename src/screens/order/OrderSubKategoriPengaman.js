import React, { Component } from "react";
import { View, Text, Container, Header, Left, Button, Icon, Body, Title, Content, Right } from "native-base";
import styles from "./styles";
import { Image, TouchableOpacity } from 'react-native';

const containerStyle = {
  borderRadius:10, 
  overflow:'hidden', 
  height:100, 
  width:100, 
  alignItems:'center',
  alignSelf: 'stretch', 
  justifyContent:'center',
  marginRight:20
};

const textStyle = {
  textAlign:'center', 
  marginRight:20
};

const imageStyle = {
  overflow:'hidden', 
  height:80, 
  width:80, 
};

const engselColor = {
  backgroundColor:'dodgerblue', 
};

const handleColor = {
  backgroundColor:'#0FBC66', 
};

const gembokColor = {
  backgroundColor:'#0FBFFF', 
};

const pagarColor = {
  backgroundColor:'#FFFCCC', 
};

const engselImage = require('../../../assets/engsel.png');
const handleImage = require('../../../assets/handle.png');
const gembokImage = require('../../../assets/gembok.png');
const pagarImage = require('../../../assets/pagar.png');

export default class OrderSubKategoriPengaman extends Component {
    render() {
      return (
        <Container style={styles.container}>
          <Header>
            <Left>
              <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>FAS - BTPNS</Title>
            </Body>
            <Right />
          </Header>
  
          <Content padder>
            <View style={{flexDirection:'row'}}>        
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('OrderForm', {
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 68,
                        sub_kategori_item_perbaikan_id: 72
                    })}>
                    <View style={[containerStyle, handleColor]}>
                        <Image style={imageStyle} source={handleImage} />
                    </View>
                    <Text style={textStyle}>Handle</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderForm', {
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 68,
                        sub_kategori_item_perbaikan_id: 73
                    })}>
                    <View style={[containerStyle, engselColor]}>
                        <Image style={imageStyle} source={engselImage} />
                    </View>
                    <Text style={textStyle}>Engsel</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderForm', {
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 68,
                        sub_kategori_item_perbaikan_id: 74
                    })}>
                    <View style={[containerStyle, gembokColor]}>
                        <Image style={imageStyle} source={gembokImage} />
                    </View>
                    <Text style={textStyle}>Gembok</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderForm', {
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 68,
                        sub_kategori_item_perbaikan_id: 75
                    })}>
                    <View style={[containerStyle, pagarColor]}>
                        <Image style={imageStyle} source={pagarImage} />
                    </View>
                    <Text style={textStyle}>Pagar/Pintu</Text>
                </TouchableOpacity>
            </View>
          </Content>
        </Container>
      );
    }
}