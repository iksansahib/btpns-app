import React, { Component } from "react";
import { View, Text, Container, Header, Left, Button, Icon, Body, Title, Content, Right } from "native-base";
import styles from "./styles";
import { Image, TouchableOpacity } from 'react-native';

const containerStyle = {
  borderRadius:10, 
  overflow:'hidden', 
  height:100, 
  width:100, 
  alignItems:'center',
  alignSelf: 'stretch', 
  justifyContent:'center',
  marginRight:20
};

const textStyle = {
  textAlign:'center', 
  marginRight:20
};

const imageStyle = {
  overflow:'hidden', 
  height:80, 
  width:80, 
};

const sumurColor = {
  backgroundColor:'dodgerblue', 
};

const wastafelColor = {
  backgroundColor:'#0FBC66', 
};

const keranColor = {
  backgroundColor:'#e05a26', 
};

const sumurImage = require('../../../assets/sumur.png');
const wastafelImage = require('../../../assets/wastafel.png');
const keranImage = require('../../../assets/keran.png');

export default class OrderSubKategoriAir extends Component {
    render() {
      return (
        <Container style={styles.container}>
          <Header>
            <Left>
              <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>FAS - BTPNS</Title>
            </Body>
            <Right />
          </Header>
  
          <Content padder>
            <View style={{flexDirection:'row'}}>        
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('OrderForm', {
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 70,
                        sub_kategori_item_perbaikan_id: 78
                    })}>
                    <View style={[containerStyle, sumurColor]}>
                        <Image style={imageStyle} source={sumurImage} />
                    </View>
                    <Text style={textStyle}>Mesin / Sumur</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderForm', {
                    kategori_id: 66,
                    kategori_item_perbaikan_id: 70,
                    sub_kategori_item_perbaikan_id: 79
                })}>
                    <View style={[containerStyle, wastafelColor]}>
                        <Image style={imageStyle} source={wastafelImage} />
                    </View>
                    <Text style={textStyle}>Wastafel / Bak</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderForm',{
                        kategori_id: 66,
                        kategori_item_perbaikan_id: 70,
                        sub_kategori_item_perbaikan_id: 80
                    })}>
                    <View style={[containerStyle, keranColor]}>
                        <Image style={imageStyle} source={keranImage} />
                    </View>
                    <Text style={textStyle}>Keran</Text>
                </TouchableOpacity>
            </View>
          </Content>
        </Container>
      );
    }
}