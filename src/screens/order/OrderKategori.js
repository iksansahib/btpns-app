import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  View,
  Text
} from "native-base";
import { AsyncStorage, Image, TouchableOpacity } from 'react-native';

import styles from "./styles";

const bikeImage = require('../../../assets/bike3.png');
const homeImage = require('../../../assets/home3.png');
const containerStyle = {
    borderRadius:10, 
    overflow:'hidden', 
    height:100, 
    width:100, 
    alignItems:'center',
    alignSelf: 'stretch', 
    justifyContent:'center',
    marginRight:20
};

const textStyle = {
  alignItems:'center',
  alignSelf: 'stretch', 
  justifyContent:'center',
};

const cadetBlue = {
  backgroundColor: 'cadetblue'
}

const warna = {
  backgroundColor: 'coral'
}

const imageStyle = {
    overflow:'hidden', 
    height:80, 
    width:80, 
};

class OrderKategori extends Component {
  async componentWillMount(){
    const logged_in = await AsyncStorage.getItem('ft_id');
    if(logged_in==null){
      this.props.navigation.navigate('Setting');
    }
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="ios-menu" />
            </Button>
          </Left>
          <Body>
            <Title>FAS - BTPNS</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
          <View style={{flexDirection:'row'}}>        
            <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderKategoriBikeItem')}>
                <View style={[containerStyle, cadetBlue]}>
                    <Image style={imageStyle} source={bikeImage} />
                </View>
                <Text style={{textAlign:'center', marginRight:20}}>Motor</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderKategoriHomeItem')}>
                <View style={[containerStyle, warna]}>
                    <Image style={imageStyle} source={homeImage} />
                </View>
                <Text style={{textAlign:'center', marginRight:20}}>Wisma</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}


export default OrderKategori;