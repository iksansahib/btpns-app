import React, { Component } from 'react';
import { Text, Container, Header, Left, Button, Icon, Body, Title, Right, Content, Card, CardItem, Form, Item, Input, Label, View, List, ListItem } from 'native-base';
import { AsyncStorage } from 'react-native';
import Axios from 'axios';
import { url_api } from '../config';
import Modal from 'react-native-modal';

const modalContentStyle = {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
}
  
export default class Setting extends Component{
    constructor(){
        super();
        this.state = {
            email: null
        }
        this.handleEmail = this.handleEmail.bind(this);
    }
    componentDidMount(){
        AsyncStorage.getItem('email')
            .then(email => {
                this.setState({email});
            })
            .catch(err=>alert(err));
    }
    handleEmail(value){
        this.setState({email:value});
    }
    renderSetting(){
        if(this.state.email!=null){
            return <SettingList handleEmail={this.handleEmail} />
        } else {
            return <LoginForm handleEmail={this.handleEmail} />
        }
    }
    render(){
        return (
            <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
                    <Icon name="ios-menu" />
                    </Button>
                </Left>
                <Body>
                    <Title>Setting</Title>
                </Body>
            </Header>
            <Content>
                {this.renderSetting()}
            </Content>
            </Container>
        )
    }
}

class LoginForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            email:'',
            password:'',
            loading: false
        }
    }
    async onLogin(){
        this.setState({loading:true});
        Axios.post(`${url_api}/api/login`,{
            email: this.state.email,
            password: this.state.password
        })
        .then(async (res)=>{
            if(res.data.success){
                await AsyncStorage.setItem("email", res.data.email);
                await AsyncStorage.setItem("m_user_id", res.data.m_user_id.toString());
                await AsyncStorage.setItem("ft_id", res.data.ft_id.toString());
                await AsyncStorage.setItem("ft_name", res.data.ft_name);
                this.setState({loading:false});
                this.props.handleEmail(res.data.email);
                alert('Login Berhasil');
            } else {
                this.setState({loading:false});
                alert(res.data.msg);
            }
        })
        .catch(err=>{
            alert(err);
            this.setState({loading:false});
        });
    }
    render(){
        return (
            <Card>
                <CardItem header bordered>
                    <Text>Login</Text>
                </CardItem>
                <CardItem>
                    <Content>
                        <Modal isVisible={this.state.loading}>
                            <View style={modalContentStyle}>
                                <Text>Loading...</Text>
                            </View>
                        </Modal>
                        <Form>
                            <Item stackedLabel>
                                <Label>Email</Label>
                                <Input onChangeText={(text) => this.setState({email:text})} keyboardType='email-address' />
                            </Item>
                            <Item stackedLabel>
                                <Label>Password</Label>
                                <Input secureTextEntry={true} onChangeText={(text) => this.setState({password:text})} />
                            </Item>
                        </Form>
                    </Content>
                </CardItem>
                <CardItem footer>
                    <Button disabled={this.state.loading} onPress={() => this.onLogin()}><Text>Login</Text></Button>
                </CardItem>
            </Card>
        )
    }
}


class SettingList extends Component{
    constructor(props){
        super(props);
        this.state = {
            ft_name: '',
            email: ''
        }
    }
    async componentDidMount(){
        email = await AsyncStorage.getItem('email');
        ft_name = await AsyncStorage.getItem('ft_name');
        this.setState({email});
        this.setState({ft_name});
    }
    async logout(){
        await AsyncStorage.removeItem("email");
        await AsyncStorage.removeItem("m_user_id");
        await AsyncStorage.removeItem("ft_id");
        await AsyncStorage.removeItem("ft_name");
        this.props.handleEmail(null);
    }
    render(){
        return(
            <List>
                <ListItem>
                    <Body>
                        <Text note>Email</Text>
                        <Text>{this.state.email}</Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Body>
                        <Text note>Nama</Text>
                        <Text>{this.state.ft_name}</Text>
                    </Body>
                </ListItem>
                <ListItem onPress={()=>this.logout()}>
                    <Body><Text>Logout</Text></Body>
                </ListItem>
            </List>
        )
    }
}