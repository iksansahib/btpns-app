
import React, { Component } from 'react';
import { Card, CardItem, Body, Text, Thumbnail, Left, Button, List, ListItem, Badge, View, Right } from 'native-base';
import { Image, Alert } from 'react-native';
import { url_api, bg } from '../../config';

class ListOrderDone extends Component{
    render(){
        if(this.props.order.kategori.m_reference_id==67){
            uri = require("../../../assets/bike2.png");
        } else {
            uri = require("../../../assets/home2.png");
        }
        if(this.props.order.image.length===0){
            file_photo = require("../../../assets/no-image.png");
        } else {
            img_name = this.props.order.image[0].image.replace('public/','');
            file_photo = {uri : 'data:image/png;base64,'+this.props.order.image[0].base64_image};
        }
        return (
            <Card key={this.props.order.t_order_id}>
                <CardItem bordered>
                    <Left>
                        <Thumbnail source={uri} />
                        <Body>
                            <Text>{this.props.order.wisma.name}</Text>
                            <Text>{this.props.order.kategori_item.name}</Text>
                            <Text note>{this.props.order.sub_kategori_item.name}</Text>
                        </Body>
                    </Left>
                    <Right><Text note>{this.props.order.created_at}</Text></Right>
                </CardItem>
                <CardItem>
                    <Body>
                        <Text>{this.props.order.deskripsi}</Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body>
                        <Badge style={{color:bg[this.props.order.status_id]}}>
                            <Text>{this.props.order.status.name}</Text>
                        </Badge>
                    </Body>
                </CardItem>
                <CardItem 
                    onPress={()=>this.props.navigation.navigate('OrderDetailDone', {
                        t_order_id: this.props.order.t_order_id
                    })} 
                    style={{backgroundColor:'#156ccc'}} 
                    button bordered footer>
                    <Text style={{width:'100%', textAlign:'center', color:'#fff'}}>LIHAT DETAIL</Text>
                </CardItem>
            </Card>
        )
    }
}
export default ListOrderDone;