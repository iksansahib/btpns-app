import React, { Component } from "react";
import { Content, Text, View, Button, Container, List, Spinner } from "native-base";
import axios from 'axios';
import { FlatList, AsyncStorage } from 'react-native';
import { url_api } from '../../config';
import ListOrderProgress from "../list/ListOrderProgress";

const modalContentStyle = {
  backgroundColor: "white",
  padding: 22,
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 4,
  borderColor: "rgba(0, 0, 0, 0.1)"
}
export default class TabTwo extends Component {
  constructor(){
    super();
    this.state = {
      orders : [],
      loading: false,
      hideSpinner: true,
      page: 0,
      loadMoreLoading: false,
      ft_id: null,
      m_user_id: null,
      refresh: false
    };
  }
  componentDidMount(){
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      this.refreshItem();
    });
    this.refreshItem();
  }
  // confirmRun(t_order_id){
  //   this.props.navigation.navigate('OrderDetail', {
  //     t_order_id: t_order_id
  //   });
  // }

  loadMore(){
    const page = this.state.page+1;
    this.setState({page: page, loadMoreLoading:true});
    axios.get(`${url_api}/api/orderprogress/${page}/20?status=1`)
      .then((res) => {
        this.setState({ 
          orders: [...this.state.orders, ...res.data], 
          loadMoreLoading:false 
        })
      }).catch((err) => {
        console.log(err);
        this.setState({loadMoreLoading:false});
      });
  }

  async refreshItem(){
    const page = 0;
    const ft_id = await AsyncStorage.getItem('ft_id');
    const m_user_id = await AsyncStorage.getItem('m_user_id');
    this.setState({ft_id});
    this.setState({m_user_id});
    this.setState({page: page, loading:true});
    axios.get(`${url_api}/api/orderprogress/${this.state.ft_id}/${page}/20?status=1`)
      .then((res) => {
        this.setState({ orders: res.data, loading:false })
      }).catch((err) => {
        this.setState({loading:false});
      });
  }
  runEksekusi(t_order_id){
    arr = [...this.state.orders];
    arr.splice(t_order_id, 1);
    this.setState({orders:arr});
  }

  buttonRefresh(){
    console.log(this.state.refresh);
    if(this.state.orders.length==0){
      return (
        <Button 
          style={{alignSelf:'center', marginTop:10}}
          onPress={() => this.refreshItem()}>
          <Text>Refresh</Text>
        </Button>
      )
    } 
  }
  renderOrders(){
    if(!this.state.loading){
      return ( 
        <FlatList 
          data={this.state.orders} 
          renderItem={({item}) => 
            <ListOrderProgress navigation={this.props.navigation} key={item.t_order_id} order={item} />}
          keyExtractor={(item,index) => item.t_order_id.toString()}
          onEndReached={this.loadMore.bind(this)}
          onRefresh={this.refreshItem.bind(this)}
          refreshing={this.state.loading}
          onEndReachedThreshold={0.1}
          extraData={this.state.refresh}
          />
      );
    } else {
      return (<Spinner />)
    }
  }
  renderLoadMoreLoading(){
    if(this.state.loadMoreLoading){
      return <Spinner />;
    }
  }
  render() {
    return (
      <List>
        {/* <Modal isVisible={this.state.loading}>
          <View style={modalContentStyle}>
              <Text>Loading Data...</Text>
          </View>
        </Modal> */}
        {this.buttonRefresh()}
        {this.renderOrders()}
        {this.renderLoadMoreLoading()}
      </List>
    );  
  }
}