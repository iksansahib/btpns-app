import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  Text,
  Right,
  Left,
  Body,
  TabHeading
} from "native-base";
import TabOne from "./tabOne";
import TabTwo from "./tabTwo";
import TabThree from "./tabThree";

class ConfigTab extends Component {
  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button 
              transparent 
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title> List Order</Title>
          </Body>
          <Right />
        </Header>
        <Tabs swipeEnabled={false} style={{ elevation: 3 }}>
          <Tab
            heading={
              <TabHeading>
                <Text>Pending</Text>
              </TabHeading>
            }
          >
            <TabOne navigation={this.props.navigation} />
          </Tab>
          <Tab
            heading={
              <TabHeading>
                <Text>On Progress</Text>
              </TabHeading>
            }
          >
            <TabTwo navigation={this.props.navigation} />
          </Tab>
          <Tab
            heading={
              <TabHeading>
                <Text>Selesai</Text>
              </TabHeading>
            }
          >
            <TabThree navigation={this.props.navigation} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
export default ConfigTab;
