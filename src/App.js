import React from "react";
import { Root } from "native-base";
import { 
  StackNavigator, 
  DrawerNavigator 
} from "react-navigation";
import ConfigTab from "./screens/tab/configTab";
import OrderKategori from "./screens/order/OrderKategori";
import OrderKategoriHomeItem from "./screens/order/OrderKategoriHomeItem";
import OrderKategoriBikeItem from "./screens/order/OrderKategoriBikeItem";
import OrderSubKategoriAir from "./screens/order/OrderSubKategoriAir";
import OrderSubKategoriListrik from "./screens/order/OrderSubKategoriListrik";
import OrderSubKategoriPengaman from "./screens/order/OrderSubKategoriPengaman";
import OrderForm from "./screens/order/OrderForm";
import OrderFormBike from "./screens/order/OrderFormBike";
import UnitPrice from "./screens/order/UnitPrice";
import UnitPriceForm from "./screens/order/UnitPriceForm";
import OrderDetail from "./screens/order/OrderDetail";
import OrderDetailProgress from "./screens/order/OrderDetailProgress";
import OrderDetailDone from "./screens/order/OrderDetailDone";
import OrderFT from "./screens/order/OrderFT";
import ListTab from "./screens/jadwal/ListTab";
import SideBar from "./screens/sidebar";
import Setting from "./screens/setting";
import CheckList from './screens/jadwal/CheckList';

const Drawer = DrawerNavigator(
  {
    OrderKategori: { screen: OrderKategori },
    ConfigTab: { screen: ConfigTab },
    ListTab: { screen: ListTab },
    Setting: { screen: Setting },
  },
  {
    initialRouteName: "OrderKategori",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = StackNavigator(
  {
    Drawer: { screen: Drawer },
    OrderKategoriHomeItem: { screen: OrderKategoriHomeItem },
    OrderKategoriBikeItem: { screen: OrderKategoriBikeItem },
    OrderSubKategoriAir: { screen: OrderSubKategoriAir },
    OrderSubKategoriListrik: { screen: OrderSubKategoriListrik },
    OrderSubKategoriPengaman: { screen: OrderSubKategoriPengaman },
    OrderForm: { screen: OrderForm },
    OrderFormBike: { screen: OrderFormBike },
    OrderDetail: { screen: OrderDetail },
    OrderDetailProgress: { screen: OrderDetailProgress },
    OrderDetailDone: { screen: OrderDetailDone },
    OrderFT: { screen: OrderFT },
    UnitPrice: { screen: UnitPrice },
    UnitPriceForm: { screen: UnitPriceForm },
    CheckList: { screen: CheckList }
  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

export default () =>
  <Root>
    <AppNavigator />
  </Root>;